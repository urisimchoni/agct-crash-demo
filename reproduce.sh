#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function cleanup {
  if [[ -z $pid ]] ; then
    return
  fi
  echo "Stopping demo server..."
  kill -HUP $pid
  while kill -0 "$pid" > /dev/null 2>&1 ; do
    sleep 0.5
  done
}

trap cleanup EXIT

$JAVA_HOME/bin/java -agentpath:$SCRIPT_DIR/binaries/libNativeAgent.so -jar $SCRIPT_DIR/binaries/oms-sb-mybatis-0.0.1-SNAPSHOT.jar &
pid=$!

sleep 5

#enable AGCT sampling
curl -sfd 'true' -H "Content-Type: application/json" http://localhost:8080/oms/agent/enable 

#make the JVM do some work
cd $SCRIPT_DIR/script
for i in `seq 1 100` ; do ./use-apis.sh ; done

