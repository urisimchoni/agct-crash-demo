#include "AgentThread.h"

jthread runAgentThread(jvmtiEnv *jvmti, JNIEnv *jni, std::string name, jvmtiStartFunction func, void *arg, jint prio)
{
	auto thrClass = jni->FindClass("java/lang/Thread");
	if (thrClass == nullptr)
	{
		printf("Failed finding Thread class\n");
		return nullptr;
	}
	auto cid = jni->GetMethodID(thrClass, "<init>", "()V");
	if (cid == nullptr)
	{
		printf("Failed finding Thread constructor method\n");
		return nullptr;
	}
	auto thread = jni->NewObject(thrClass, cid);
	if (thread == nullptr)
	{
		printf("Failed creating new Thread object\n");
		return nullptr;
	}
	auto mid = jni->GetMethodID(thrClass, "setName", "(Ljava/lang/String;)V");
	if (mid == nullptr)
	{
		printf("Failed finding Thread.setName\n");
		return nullptr;
	}
	auto threadName = jni->NewStringUTF(name.c_str());
	if (threadName == nullptr)
	{
		printf("Failed constructing thread name\n");
		return nullptr;
	}
	jni->CallObjectMethod(thread, mid, threadName);
	if (jni->ExceptionCheck())
	{
		printf("Failed setting '%s' thread name\n", name.c_str());
		return nullptr;
	}

	auto err = jvmti->RunAgentThread(thread, func, arg, prio);
	if (err != JVMTI_ERROR_NONE)
	{
		printf("Failed running agent '%s' thread: %d\n", name.c_str(), err);
		return nullptr;
	}

	return thread;
}