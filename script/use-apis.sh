#!/usr/bin/env bash

err_report() {
  echo
  echo "***** errexit on line $(caller)" >&2
}

trap err_report ERR

set -e

SERVER=${SERVER:-localhost}
SERVER_URL=${SERVER_URL:-http://${SERVER}:8080/oms}
CURL=${CURL:-curl -sf}

ORDER_ID=${ORDER_ID:-`od -A n -l -N 7 /dev/urandom | tr -d " "`}
#due to a bug this has to be the same as order ID
ORDER_LINE_ID=${ORDER_ID}
SKU=${SKU:-`od -A n -l -N 2 /dev/urandom | tr -d " "`}

#create an order
cat order.json | jq ".customerOrderId = \"${ORDER_ID}\"" | jq ".orderLines[].customerSku=\"${SKU}\"" | $CURL -X POST -H "Content-Type: application/json" -d @- $SERVER_URL/order

#get the order
$CURL $SERVER_URL/order/${ORDER_ID}

#create inventory
cat inv.json | jq ".skuId=\"${SKU}\"" | $CURL -X POST -H "Content-Type: application/json" -d @- $SERVER_URL/inventory

#get inventory
$CURL $SERVER_URL/inventory/${SKU}

#create shipping record
cat shipping.json | jq ".skuId=\"${SKU}\"" | $CURL -X POST -H "Content-Type: application/json" -d @- $SERVER_URL/shipping

#get shipping record
$CURL $SERVER_URL/shipping/${SKU}

#find stores in zip code
$CURL $SERVER_URL/store/07470

#modify fulfillment shipping-to-pickup
cat mforder.json | jq ".customerOrderId = \"${ORDER_ID}\"" | $CURL -X PATCH -H "Content-Type: application/json" -d @- $SERVER_URL/modify/fulfillment/store/items/${ORDER_LINE_ID}

#modify fulfillment pickup-to-shipping
cat mforder.json | jq ".customerOrderId = \"${ORDER_ID}\"" | $CURL -X PATCH -H "Content-Type: application/json" -d @- $SERVER_URL/modify/fulfillment/shipping/items/${ORDER_LINE_ID}

echo
echo
printf "\033[0;32mOK\033[0m\n"
