# AGCT Crash Sample

This repository contains a minimal JVM native agent to reproduce
AsyncGetCallTrace issue. It also contains a built binary of the agent,
and a sample application.


## To Reproduce

* install curl, jq, bash
* Set JAVA_HOME
* Run:
```
./reproduce.sh
```

The script runs a Spring-boot application with the agent, enables periodic
AGCT sampling, and generates some traffic. If the issue is reproduced, the JVM
crashes.

## Building

Building is only required for tweaking the sample, as binaries are
already included.

### Native Agent

* Install a C++ compiler, make, cmake
* Set CC and CXX to path of the C compiler and C++ compiler, respectively.
* Run the following:

```
./build-native.sh
```

This builds the native agent into build subdirectory.

### Java

```
./gradlew build publishToMavenLocal
```

This builds the class which allows manipulation of the native agent
and publishes it to a local maven repository. It can then be used
to start/stop the agent from an application.

(the source code of the Spring-boot application is not provided as it
would clutter the sample).

