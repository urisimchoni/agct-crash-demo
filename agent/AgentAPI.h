#include <jni.h>

#ifndef AgentAPI_H
#define AgentAPI_H
#ifdef __cplusplus
extern "C"
{
#endif
	JNIEXPORT void JNICALL Java_com_example_agctdemo_Callbacks_setSamplingEnabled(JNIEnv *jni_env, jclass, jboolean);

#ifdef __cplusplus
}
#endif
#endif
