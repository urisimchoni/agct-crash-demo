#pragma once
#include "jvmti.h"

bool probeAGCT(JavaVM *_jvm, jvmtiEnv *_jvmti, bool &supports);
bool updateAGCTSampling(JNIEnv *jni, bool enabled);
void agctOnThreadRun();
void agctOnThreadEnd();
