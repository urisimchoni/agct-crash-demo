#include "AgentAPI.h"
#include "Agct.h"
#include <string>
#include <stdio.h>
#include <atomic>

using namespace std;

static jvmtiEnv *jvmti;
static bool sampling_stacks;

static void createJMethodIDsForClass(jvmtiEnv *jvmti, jclass klass)
{
	jint method_count;
	jmethodID *methods = nullptr;
	auto err = jvmti->GetClassMethods(klass, &method_count, &methods);
	if (err != JVMTI_ERROR_NONE && err != JVMTI_ERROR_CLASS_NOT_PREPARED)
	{
		char *ksig = nullptr;
		auto kserr = jvmti->GetClassSignature(klass, &ksig, NULL);
		string className;
		if (kserr == JVMTI_ERROR_NONE)
		{
			className = ksig;
			jvmti->Deallocate((unsigned char *)ksig);
		}
		else
		{
			className = "<unknown>";
		}

		printf("Failed to create method IDs for class %s\n", className.c_str());
	}
	if (methods)
	{
		jvmti->Deallocate((unsigned char *)methods);
	}
}

JNIEXPORT void JNICALL Java_com_example_agctdemo_Callbacks_setSamplingEnabled(JNIEnv *jni_env, jclass, jboolean enable)
{
	auto error =
		jvmti->SetEventNotificationMode(enable ? JVMTI_ENABLE : JVMTI_DISABLE, JVMTI_EVENT_CLASS_PREPARE, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set event notifications: %d\n", __FUNCTION__, error);
		abort();
	}
	sampling_stacks = true;

	jint size = 0;
	jclass *loadedClasses = nullptr;

	auto err = jvmti->GetLoadedClasses(&size, &loadedClasses);
	if (err != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to get loaded classes: %d\n", __FUNCTION__, err);
		abort();
	}

	for (int i = 0; i < size; ++i)
	{
		jclass klass = loadedClasses[i];
		jint clsstatus = 0;
		err = jvmti->GetClassStatus(klass, &clsstatus);
		if (err != JVMTI_ERROR_NONE)
		{
			printf("%s\tfailed to get class status: %d\n", __FUNCTION__, err);
			continue;
		}
		if (clsstatus & JVMTI_CLASS_STATUS_PREPARED)
		{
			createJMethodIDsForClass(jvmti, klass);
		}
	}

	jvmti->Deallocate((unsigned char *)loadedClasses);

	updateAGCTSampling(jni_env, enable);
}

static void JNICALL onClassPrepare(jvmtiEnv *jvmti_env, JNIEnv *jni_env, jthread, jclass klass)
{
	if (sampling_stacks)
	{
		createJMethodIDsForClass(jvmti_env, klass);
	}
}

void JNICALL onClassLoad(jvmtiEnv *jvmti_env, JNIEnv *jni_env, jthread thread, jclass klass)
{
	// Implicitly needed by AsyncGetCallTrace()
}

static void JNICALL onThreadStart(jvmtiEnv *jvmti_env, JNIEnv *jni_env, jthread thread)
{
	agctOnThreadRun();
}

static void JNICALL onThreadEnd(jvmtiEnv *jvmti_env, JNIEnv *jni_env, jthread thread)
{
	agctOnThreadEnd();
}

JNIEXPORT jint JNICALL Agent_OnLoad(JavaVM *jvm, char *options, void *reserved)
{
	jvmtiEventCallbacks callbacks = {};
	jvmtiError error;

	jint result = jvm->GetEnv((void **)&jvmti, JVMTI_VERSION_1_1);
	if (result != JNI_OK)
	{
		printf("%s\tUnable to access JVMTI!: %d\n", __FUNCTION__, result);
	}
	else
	{
		printf("Agent succesfully loaded\n");
	}

	bool agctSupported = false;
	if (!probeAGCT(jvm, jvmti, agctSupported) || !agctSupported)
	{
		return JNI_ERR;
	}

	// jvmtiCapabilities capa = {};

	// capa.can_generate_all_class_hook_events = 1;
	// error = jvmti->AddCapabilities(&capa);
	// if (error != JVMTI_ERROR_NONE)
	// {
	// 	printf("%s\tfailed to set capabilities: %d\n", __FUNCTION__, error);
	// }

	callbacks.ClassPrepare = onClassPrepare;
	callbacks.ClassLoad = onClassLoad;
	callbacks.ThreadStart = onThreadStart;
	callbacks.ThreadEnd = onThreadEnd;

	error = jvmti->SetEventCallbacks(&callbacks, sizeof(callbacks));
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set event callbacks: %d\n", __FUNCTION__, error);
	}

	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_CLASS_LOAD, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set class load: %d\n", __FUNCTION__, error);
		return JNI_ERR;
	}
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_THREAD_START, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set thread start: %d\n", __FUNCTION__, error);
	}
	error = jvmti->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_THREAD_END, nullptr);
	if (error != JVMTI_ERROR_NONE)
	{
		printf("%s\tfailed to set thread end: %d\n", __FUNCTION__, error);
	}

	return JNI_OK;
}
