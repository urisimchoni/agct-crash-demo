#!/usr/bin/env bash

set -e
set -x
#
# RUN FROM PROJECT ROOT!
#
# prerequisites: JAVA_HOME pointing to a JDK
#
rm -rf build
mkdir build
cd build
cmake ../agent
make

