#include "StackRing.h"

using namespace std;

// To make ring size adjustable at runtime, the atomic has
// to be wrapped, as in https://stackoverflow.com/a/13194652/4901909
StackRing::StackRing(size_t ringSize, size_t sSz) : ring(ringSize)
{
	static_assert(atomic<int>::is_always_lock_free);

	stackSize = sSz;
	for (auto &r : ring)
	{
		r.stack.resize(stackSize);
	}
}

StackRing::Slot *StackRing::getPushSlot()
{
	int a = addIdx.load(memory_order_relaxed);
	int p = popIdx.load(memory_order_acquire);
	while (true)
	{
		auto nextIdx = advanceIdx(a);
		if (nextIdx == p)
		{
			return nullptr;
		}

		if (addIdx.compare_exchange_weak(a, nextIdx, memory_order_relaxed))
		{
			break;
		}
	}

	return &ring[a];
}

StackFrames *StackRing::getPopPoint()
{
	int p = popIdx.load(memory_order_relaxed);
	int a = addIdx.load(memory_order_relaxed);
	if (p == a)
	{
		return nullptr;
	}

	auto &slot = ring[p];
	if (!slot.dataAvail.load(memory_order_acquire))
	{
		return nullptr;
	}

	return &slot.stack;
}

void StackRing::advancePopPoint()
{
	int p = popIdx.load(memory_order_relaxed);
	auto &slot = ring[p];
	slot.stack.resize(stackSize);
	slot.dataAvail.store(false, memory_order_relaxed);
	p = advanceIdx(p);
	popIdx.store(p, memory_order_release);
}

int StackRing::advanceIdx(int idx)
{
	return (idx + 1) % ring.size();
}
