#pragma once
#include <vector>
#include <atomic>
#include <jvmti.h>

typedef std::vector<jmethodID> StackFrames;

//
// This is a lockless multi-producer-single-consumer ring
// of call stacks.
//
// "Producing" consists of getting a slot using getPushSlot(),
// then filling the stack in the slot, and calling slot->produce().
//
// "Consuming" consists of getting a pointer to the next produced stack
// using getPopPoint(), then using the data in the stack (this may include
// moving), and finally, to signal that we're done with this stack and
// replenish the (potentially moved-from) slot, calling advancePopPoint().
//
// The consumer can be multi-threaded, but it has to externally
// guarantee that no two threads consume at the same time (they don't
// have to provide memory barriers). In addition, each "produce" or "consume"
// sequence has to occur within the same thread or signal handler.
//
// This ring has global push and pop indices, as well as per-slot "data available"
// flag. The invariant that we keep is that if the indices indicate that a slot can be
// pushed-to, then the slot's "data available" flag is false. This way finding a slot
// to push into involves just atomic operations on a single variable.
//
// On the pop side, we have a single consumer, so it has to verify first that a slot can
// be popped-from (according to indices) and then that data is available.
//
class StackRing
{
public:
	class Slot
	{
	public:
		StackFrames &getStack() { return stack; }
		void produce() { dataAvail.store(true, std::memory_order_release); }

	private:
		friend class StackRing;
		std::atomic<bool> dataAvail = false;
		StackFrames stack;
	};
	StackRing(size_t ringSize, size_t sSz);
	Slot *getPushSlot();
	StackFrames *getPopPoint();
	void advancePopPoint();

private:
	int advanceIdx(int idx);
	size_t stackSize;
	std::vector<Slot> ring;

	// only the producers modify addIdx, and only the consumer
	// modifies popIdx.
	//
	// - The atomicity of addIdx supports cmpxchg(), and prevents
	//   data dependencies (accessing the dataAvail before we know
	//   the slot is ours)
	// - the slot "dataAvail" member provides a memory barrier ensuring a new stack is fully saved before
	//   it is consumed.
	// - popIdx provides a memory barrier ensuring the slot is replenished before accessing it.
	std::atomic<int> addIdx = 0;
	std::atomic<int> popIdx = 0;
};