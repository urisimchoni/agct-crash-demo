#include "Agct.h"
#include "StackRing.h"
#include "AgentThread.h"
#include <atomic>
#include <unordered_map>
#include <mutex>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <dlfcn.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <string>

using namespace std;

struct AgctFrame
{
	jint bci;
	jmethodID method;
};

struct AgctSample
{
	JNIEnv *jni;
	jint frameCount;
	AgctFrame *frames;
};

typedef void (*AGCTType)(AgctSample *, jint, void *);

const int AGCT_MAX_FRAMES = 640;
const size_t AGCT_RING_SIZE = 128;
const int AGCT_INTERVAL_MSEC = 3;

static AGCTType AsyncGetCallTrace;
static JavaVM *jvm;
static jvmtiEnv *jvmti;
static bool thread_initialized;
static atomic<bool> stopping;
int curTimerInterval;
#if defined(HAVE_THREAD_TIMER_API)
typedef pid_t timer_thread_id_t;
#else
typedef __pid_t timer_thread_id_t;
#endif
static unordered_map<timer_thread_id_t, timer_t> thread_timers;
static mutex thread_timers_mutex;

static sigset_t sigProfMask;

// Single producer - signal handler(there's a guarantee not to
// deliver another non-RT signal while the handler of the same
// signal runs)
//
// Single consumer - collecting thread
static StackRing *sampled_stacks;
static int sync_fds[2];

// Get a JNI interface for current thread. This runs
// from within a signal handler, so only async-safe code
// please.
static JNIEnv *getJNI()
{
	JNIEnv *jni;
	auto rc = jvm->GetEnv((void **)&jni, JNI_VERSION_1_6);
	if (rc != 0)
	{
		return nullptr;
	}

	return jni;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result";
// Wake thread if it's sleeping
static void wakeCollectingThread()
{
	uint8_t wr = 1;
	// write() can only fail if the pipe is full, and
	// in that case the thread will be woken up.
	write(sync_fds[1], &wr, sizeof(wr));
}

// Wait till there *might* be some action
// to do.
static void collectingThreadWait()
{
	uint8_t rd[16];
	// we don't care about the result of read nor
	// about the data it produced - the data is just
	// bytes that were stuffed by the waking thread.
	// if we don't clean up the pipe, it will be cleaned
	// up next time. Read returns when there *might* be
	// data to process.
	read(sync_fds[0], rd, sizeof(rd));
}

#pragma GCC diagnostic pop

static void signalHandler(int signum, siginfo_t *info, void *context)
{
	auto jni = getJNI();
	if (jni == nullptr)
	{
		// non-Java thread
		return;
	}

	AgctFrame frames[AGCT_MAX_FRAMES];
	AgctSample sample = {};
	sample.frames = frames;
	sample.jni = jni;

	AsyncGetCallTrace(&sample, AGCT_MAX_FRAMES, context);
	if (sample.frameCount <= 0)
	{
		return;
	}

	// jmethodID *parentStack;
	// size_t parentSize = getCurThreadParent(&parentStack);
	// if (parentSize > 0)
	// {
	// 	// Shave-off Thread.Run() from current thread
	// 	sample.frameCount -= 1;
	// }

	if (size_t(sample.frameCount) >= AGCT_MAX_FRAMES)
	{
		return;
	}

	// if (size_t(sample.frameCount) + parentSize > AGCT_MAX_FRAMES)
	// {
	// 	return;
	// }

	auto slot = sampled_stacks->getPushSlot();
	if (slot == nullptr)
	{
		return;
	}

	// This resize shrinks the vector and is therefore signal-safe
	//(shrinking is guaranteed not to invalidate iterators, therefore
	// implicitly not to re-allocate)
	auto &slot_frames = slot->getStack();
	// slot_frames.resize(sample.frameCount + parentSize);
	slot_frames.resize(sample.frameCount);
	for (auto idx = 0; idx < sample.frameCount; idx++)
	{
		slot_frames[idx] = frames[idx].method;
	}
	// std::copy(parentStack, parentStack + parentSize, &slot_frames[sample.frameCount]);

	slot->produce();

	wakeCollectingThread();
}

static void collectingThread(jvmtiEnv *jvmti_env, JNIEnv *jni_env, void *arg)
{
	auto drained = true;
	while (true)
	{

		if (drained)
		{
			collectingThreadWait();
			drained = false;
		}

		auto frames = sampled_stacks->getPopPoint();
		if (!frames)
		{
			drained = true;
			stopping = false;
			continue;
		}

		// For some reason, AsyncGetCallTrace() sometimes fails to resolve, i.e.
		// attach a valid method-id, to the bottom-of-stack frame, and instead
		// puts a frame with null method ID. The bottom-of-stack is typically
		// Thread.Run(), which is filtered anyway in post-processing stage.
		// Therefore if we simply remove the bottom-of-stack frame, then two
		// identical stacks, one with peeled-off BOS and one without, will
		// become identical and be merged during post-processing (assuming the
		// BOS is filtered).
		//
		// An alternative, which we used with honest-profiler, is to drop a
		// stack sample containing a null method-id, but that causes tests to
		// fail.
		if (!frames->empty() && frames->back() == nullptr)
		{
			frames->resize(frames->size() - 1);
		}

		StackFrames stk(std::move(*frames));
		frames->clear();
		sampled_stacks->advancePopPoint();

		// Do something with stk
	}
}

static bool initSampling(JNIEnv *jniEnv)
{
	auto ringSize = AGCT_RING_SIZE;
	sampled_stacks = new StackRing(ringSize, AGCT_MAX_FRAMES);

	auto thread = runAgentThread(
		jvmti, jniEnv, "vFunction Stack Collection Thread", collectingThread, nullptr, JVMTI_THREAD_NORM_PRIORITY);
	if (thread == nullptr)
	{
		printf("Failed creating collecting thread\n");
		return false;
	}

	struct sigaction sa;
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = NULL;
	sa.sa_sigaction = signalHandler;
	sa.sa_flags = SA_RESTART | SA_SIGINFO;

	sigaction(SIGPROF, &sa, NULL);

	return true;
}

bool probeAGCT(JavaVM *_jvm, jvmtiEnv *_jvmti, bool &supports)
{
	static_assert(atomic<uint64_t>::is_always_lock_free);

	void *sym = dlsym(RTLD_DEFAULT, "AsyncGetCallTrace");
	if (sym == nullptr)
	{
		printf("AsyncGetCallTrace not available\n");
		supports = false;
		return true;
	}

	AsyncGetCallTrace = reinterpret_cast<AGCTType>(sym);
	jvm = _jvm;
	jvmti = _jvmti;

	auto res = pipe(sync_fds);
	if (res < 0)
	{
		printf("Failed creating sync pipe - %s\n", strerror(errno));
		return false;
	}
	int flags = fcntl(sync_fds[1], F_GETFL, 0);
	if (flags < 0)
	{
		printf("Failed getting pipe write side flags - %s\n", strerror(errno));
		return false;
	}
	flags |= O_NONBLOCK;
	res = fcntl(sync_fds[1], F_SETFL, flags);
	if (res < 0)
	{
		printf("Failed setting pipe write to non-blocking - %s\n", strerror(errno));
		return false;
	}

	sigemptyset(&sigProfMask);
	sigaddset(&sigProfMask, SIGPROF);

	printf("AsyncGetCallTrace is available\n");
	supports = true;
	return true;
}

static bool setTimer(timer_t tm, int intervalMS)
{
	itimerspec ts;
	ts.it_interval.tv_sec = 0;
	ts.it_interval.tv_nsec = intervalMS * 1000000;
	ts.it_value = ts.it_interval;
	if (timer_settime(tm, 0, &ts, nullptr) != 0)
	{
		printf("Error setting a timer to %d ms - %s\n", intervalMS, strerror(errno));
		return false;
	}

	return true;
}

static bool setAllTimers(int intervalMS)
{
	auto ok = true;
	lock_guard<mutex> lck(thread_timers_mutex);
	curTimerInterval = intervalMS;
	for (auto &entry : thread_timers)
	{
		if (!setTimer(entry.second, curTimerInterval))
		{
			ok = false;
		}
	}

	return ok;
}

bool updateAGCTSampling(JNIEnv *jni, bool enabled)
{
	if (enabled)
	{
		if (!thread_initialized)
		{
			if (!initSampling(jni))
			{
				return false;
			}

			thread_initialized = true;
		}

		return setAllTimers(AGCT_INTERVAL_MSEC);
	}
	else
	{
		auto ok = setAllTimers(0);

		if (thread_initialized)
		{
			//
			// If the signal handler is being run now
			// by another CPU, we might get leftover
			// events. Only way I know of to wait till
			// a possibly running signal handler has finished
			// is to sleep for a while - hopefully any running
			// signals will complete by the time we finish sleeping.
			//
			timespec us = {0, 50000};
			nanosleep(&us, nullptr);

			// Ensure thread collects everything before returning
			stopping = true;
			wakeCollectingThread();
			us = {0, 1000};
			while (stopping)
			{
				nanosleep(&us, nullptr);
			}
		}

		return ok;
	}
}

void agctOnThreadRun()
{
	// initTLSParentStack();
	// initContextTrackers();

	// std::atomic_signal_fence(std::memory_order_seq_cst);

	timer_thread_id_t curTID;
	sigevent sev{};
	sev.sigev_notify = SIGEV_THREAD_ID;
	sev.sigev_signo = SIGPROF;
#if defined(HAVE_THREAD_TIMER_API)
	curTID = gettid();
	sev.sigev_notify_thread_id = curTID;
#else
	curTID = syscall(__NR_gettid);
	sev._sigev_un._tid = curTID;
#endif
	timer_t tm;
	if (timer_create(CLOCK_THREAD_CPUTIME_ID, &sev, &tm) != 0)
	{
		printf("Error creating a new thread timer (%zd timers exist) - %s\n", thread_timers.size(), strerror(errno));
		return;
	}

	{
		lock_guard<mutex> lck(thread_timers_mutex);
		auto ins = thread_timers.emplace(curTID, tm);
		if (!ins.second)
		{
			printf("a timer for thread %d already exists in the list!!!\n", curTID);
			timer_delete(tm);
			return;
		}

		if (curTimerInterval != 0)
		{
			setTimer(tm, curTimerInterval);
		}
	}
}

void agctOnThreadEnd()
{
#if defined(HAVE_THREAD_TIMER_API)
	timer_thread_id_t curTID = gettid();
#else
	timer_thread_id_t curTID = syscall(__NR_gettid);
#endif

	timer_t tm;
	{
		lock_guard<mutex> lck(thread_timers_mutex);
		auto it = thread_timers.find(curTID);
		if (it == thread_timers.end())
		{
			printf("a timer for thread %d was not created!!!\n", curTID);
			return;
		}
		tm = it->second;
		thread_timers.erase(it);
	}

	setTimer(tm, 0);

	if (timer_delete(tm) != 0)
	{
		printf("Error deleting timer for thread %d - %s\n", curTID, strerror(errno));
	}
}
