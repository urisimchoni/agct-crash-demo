#pragma once
#include <string>
#include <jvmti.h>

jthread runAgentThread(jvmtiEnv *jvmti, JNIEnv *jni, std::string name, jvmtiStartFunction func, void *arg, jint prio);